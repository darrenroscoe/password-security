$(document).ready(function() {
    var hasNumber = /\d/,
        hasLowercase = /[a-z]/,
        hasUppercase = /[A-Z]/,
        hasSpecialChars = /[!@#$%^&*()~'"`:;?+=-_]/,
        // how many milliseconds it takes to try one password
        desktopRate = 17,
        GPURate = desktopRate / 100,
        botnetRate = desktopRate / 100000;


    $('#pw-input').val("").change(function(evt) {
        var pword = evt.target.value;
        if (pword === "") {
            $('#desktop-time').empty();
            $('#gpu-time').empty();
            $('#botnet-time').empty();
            return
        }
        if (pword === "password") {
            $('#desktop-time').html("Instantly");
            $('#gpu-time').html("Instantly");
            $('#botnet-time').html("Instantly");
            return
        }
        var charset = 0;
        if (hasNumber.test(pword)) {
            charset += 10;
        }
        if (hasLowercase.test(pword)) {
            charset += 26;
        }
        if (hasUppercase.test(pword)) {
            charset += 26;
        }
        if (hasSpecialChars.test(pword)) {
            charset += 21;
        }
        complexity = Math.pow(charset, pword.length);
        var desktopTime = moment.duration(complexity * desktopRate / 2),
            gpuTime = moment.duration(complexity * GPURate / 2),
            botnetTime = moment.duration(complexity * botnetRate / 2);
        $('#desktop-time').html(desktopTime.humanize());
        $('#gpu-time').html(gpuTime.humanize());
        $('#botnet-time').html(botnetTime.humanize());
    });
});
