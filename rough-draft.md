For rendered viewing go to https://gitlab.com/darrenroscoe/password-security/blob/master/rough-draft.md

# Passwords, The Keys To Everything

Are you really who you say you are? That is the question passwords attempt to answer. When you choose a password you are choosing a key to fit the lock you are logging in to, but according to a 2015 [survey by TeleSign][1] most Americans are using these keys in such a way that leaves them extremely vulnerable. Lately it seems like every few weeks there is another story in the news about a big data breach where thousands or millions of users passwords are leaked onto the black market, and these are only the breaches that data owners find out about. The following is an explanation of how your passwords can end up in the wrong hands and what you can do about it.

## What is a password and how is it used?

This may seem like a simple question, but the answer is not obvious from a technical perspective. When you enter your password, the system you are logging in to must be able to confirm that the password you just entered and the password you entered when signing up (or last changing your password) are the same. One way to do that is to simply store the password the user enters when signing up and when the user logs in compare the entered password to the known password. This method has one horrendous flaw, however, because if a malicious party gains access to the store of passwords then all user accounts are instantly compromised.

In order to avoid this, we need a way to confirm the passwords are the same without storing any passwords. How does this happen? With a little something called Hashing. Hashing is essentially the process of taking a password and using it to generate a number in a certain range so that if one knows the password they can easily find the number, but if one knows the number it is nearly impossible to find out the password. In order to find the password that corresponds to a certain number, there is no better way than to try Hashing every possible password until one happens to Hash into the right number, essentially guess and check. Using this method even when a malicious party gains access to the store of "passwords" they still may not have access to the password themselves. This is because it is possible to design a Hashing strategy such that a sufficiently complex password would take longer than the life of the universe so far for a computer to guess correctly.

The details of how Hashing works and other measures to take in conjunction with Hashing are beyond the scope of this post, but the point here is that the strength and security of a password is based on how easy it is to guess.

## How to guess a password

To understand how to prevent your password from being guessed you'll need to understand the different guessing techniques and how to thwart each one in turn.

### Brute Force

The simplest form of guessing is knows as brute force, as in you guess the right answer simply by the brute force of how many guesses you make. This is essentially the technique of guessing a 4-digit pin by first guessing 0000, then 0001, then 0002 and so on. The success of this method depends on a number of factors, including how many and how powerful the guessing computers are, how long it takes to know whether a single guess is correct or not, and other more complex details regarding the specific Hashing algorithm and other measures taken. Most of these factors are out of your control, but there a few very important factors that you *do* control: the length and complexity of your password. Put simply, the longer a password is, and the more possible symbols each characters in the password could be, the longer it will take to guess the password. According to [some estimates][2] a difference of 4 characters can mean a difference in time-to-guess of 2 centuries!

### Dictionary Attack

A step up from brute force is the Dictionary Attack, wherein the attacker uses a list of common passwords and words in order to increase the chance of guessing someones real password. This type of attack can be mitigated by not choosing obvious passwords, such as "12345" and "password". There are a surprising number of passwords on these lists of common passwords, so while it can seem clever to use something like "P@$svv0Rd", someone has inevitably tried that already, and it is probably somewhere on the list.

### Social Engineering

This method encompasses a broad collection of ways a human attacker can use external information to get password. One way is if you choose a password based on some information about yourself such as a birth date or family member or pet's name or birth date, a human who knows this information about you may be able to guess your password. Another way might be to trick a user into revealing their password by posing as a trusted party. There are many ways this type of attack can occur, but to mitigate this is relatively easy. Simply avoid choosing obvious passwords derived from personal information, and never ever reveal your password to anyone, especially not over a textual medium.

## How to make an amazing password

With all of these ways of guessing (and more complex ones not covered here) there are a lot of things to be careful of when choosing a password. Unfortunately the most difficult to guess passwords tend to also be some of the most difficult to remember, which somewhat erodes the value of the password in the first place. In addition, having multiple long complex passwords quickly becomes a nightmare so many people when they decide on a good password simply reuse that password everywhere. The problem then becomes that no two sites have the same level of security regarding their passwords, and if one of those sites is vulnerable enough that an attacker can access your password through it, that attacker now has access to your account on all of those other sites. In order to mitigate these issues, follow these guidelines when creating a password.

### Don't use a Pass *word* use a Pass *phrase*

Often the word "password" subconsciously implies it should be a single word, but really the best way to make an easy to remember password is to us multiple (seemingly random) words in a phrase that you then give meaning to. A great example of this is the [correct horse battery staple](https://xkcd.com/936/) xkcd comic. The human mind is much better at remembering phrases than sets of characters, and using multiple words makes it easy to make it very long.

### Add a few special characters to the Passphrase

By adding even one or two non-letter characters to your passphrase you drastically increase the entropy (difficulty of guessing) that passphrase, especially if the characters appear somewhere other than a word boundary. This is because if the passphrase is only letters, a dictionary attack guessing random word combinations will miss passwords with special characters randomly inserted, and trying random word combinations and random special characters and locations amounts to an infeasible number of guesses. A good example of this would be something like "corr$ecthorsebat9terystaple" to add to the example above. It becomes slightly harder to remember, but a good mnemonic solves that problem.

### Change periodically

The longer you use any given Passphrase the more chances someone has to get it from you (usually without your knowledge such as by logging keystrokes on a computer you don't normally use) and once they do that it loses all of its value. In order to protect against this you can change your password as frequently as you feel the need to. Depending on how careful you are with it this frequency may vary.

### Only remember one Passphrase

That's right. Think of one really solid Passphrase, commit it to memory, and remember only that ever (until you change it). You may be saying "I thought I was supposed to use a different password for every site?" and that is still true, but remembering all of those passwords is infeasible for our measly mortal minds. Instead the best option is to turn to a password manager such as [LastPass](https://www.lastpass.com/). With a password manager you choose one really strong master passphrase and the password manager uses that to encrypt and store your passwords for other websites. Password managers use encryption so that all of your other passwords are utterly inaccessible without your master password, so use the above strategies to choose a really long, strong, and easy to remember passphrase for the master password. Once you have a password manager, you can use it to generate extremely strong passwords for other websites, so that instead of remembering your passwords for every site you use, you simply remember the master password, log into the password manager, and get from there the unique strong password you use for each site. By randomly generating a password for each site you make the password for each site really hard to guess, and since each site has a unique password if a site has bad security practices (i.e. stores passwords as plain text without Hashing) and your account on that site is compromised, none of your other accounts are affected.


## It's not over, it's never over.

Unfortunately no matter how many precautions you take and how long and arbitrary your master password is (mine is over 30 characters) there is always room to miss something. Maybe you accidentally type your master password into a phishing site, maybe your password manager of choice has a security flaw nobody knew about until some hacker exposed it to the world in a big data breach (rare but awful), or maybe you are just too proud of it and brag a little too iloundly and specifically about your great new password. However it happens it's always possible to be more vulnerable than you realize, so to really stay protected, there are methods beyond passwords that add even more security at the cost of some convenience, such as Multi-factor Authentication (MFA), which you should look into once you implement the above advice.

[1]: https://blog.hubspot.com/marketing/password-statistics
[2]: https://www.betterbuys.com/estimating-password-cracking-times/
